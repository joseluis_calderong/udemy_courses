#!/bin/bash


if [ $# -ne 2 ]; then
   echo "Usage: bash " $0 " <file_with_list_of_file_names> <target_dir>"
   exit 0
fi

FILE=$1
TARGET_DIR=$2

echo "Copying jar files..."


while read line
do
   echo "Processing file: " $line

   cp $line $TARGET_DIR
done<$FILE

echo "All jars in file " $1 " have been copied to " $TARGET_DIR



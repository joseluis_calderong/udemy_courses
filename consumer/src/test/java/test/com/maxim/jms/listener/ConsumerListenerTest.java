package test.com.maxim.jms.listener;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.maxim.jms.listener.ConsumerListener;
import com.mongodb.MongoException;

import test.com.maxim.jms.mocks.TextMessageMock;

public class ConsumerListenerTest {

	private TextMessage message;
	private TextMessageMock messageMock;
	
	private ConsumerListener listener;
	private ApplicationContext context;
	
	private String json = "{vendorName:\"Microsofttest3\",firstName:\"BobTest3\",lastName:\"SmithTest3\",address:\"123 Main test3\",city:\"TulsaTest3\",state:\"OKTest3\",zip:\"71345Test3\",email:\"Bob@microsoft.test3\",phoneNumber:\"test-123-test3\"}";
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener) context.getBean("consumerListener");
		message = createMock(TextMessage.class);
		messageMock = createMock(TextMessageMock.class);
	}

	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext) context).close();
	}

	@Test
	public void testOnMessage() throws JMSException {
		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
		verify(message);
	}
	
	@Test
	public void testOnMessageJMSException() throws JMSException {
		expect(message.getText()).andStubThrow(new JMSException("testOnMessageJMSException"));
		replay(message);
		
		try {
			listener.onMessage(message);
		} catch (Exception e) {}
		
		verify(message);
	}

	@Test
	public void testOnMessageMongoException() throws MongoException, JMSException {
		expect(message.getText()).andStubThrow(new MongoException("testOnMessageMongoException"));
		replay(message);
		
		try {
			listener.onMessage(message);
		} catch (Exception e) {}
		
		verify(message);
	}
	
	@Test
	public void testOnMessageException() throws Exception {
		expect(message.getText()).andStubThrow(new RuntimeException("testOnMessageException"));
		replay(message);
		
		try {
			listener.onMessage(message);
		} catch (Exception e) {}
		
		verify(message);
	}
	
	@Test
	public void testOnMessageNotInstanceOfTextMessage() throws JMSException {
		expect(messageMock.getText()).andReturn(json);
		replay(messageMock);
		try {
			listener.onMessage(messageMock);
		} catch (Exception e) {}
		//verify(myMessage);
	}
}

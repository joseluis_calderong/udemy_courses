package test.com.maxim.jms.mocks;

import javax.jms.Message;

public interface TextMessageMock extends Message {
	public String getText();

}

package test.com.maxim.jms.adapter;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.maxim.jms.adapter.ConsumerAdapter;
import com.mongodb.MongoException;
import com.mongodb.util.JSONParseException;

public class ConsumerAdapterTest {

	private String json = "{vendorName:\"Microsofttest\",firstName:\"BobTest\",lastName:\"SmithTest\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}";
	private String json2 = "xxxx";
	private ConsumerAdapter adapter;
	
	@Before
	public void setUp() throws Exception {
		adapter = new ConsumerAdapter();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToMongoDB() throws MongoException, Exception {
		adapter.sendToMongoDB(json);
		assertNotNull(json);
	}
	
	@Test
	public void testSendToMongoDBJSONParseException() throws JSONParseException {
		try {
			adapter.sendToMongoDB(json2);
		} catch (Exception e) {}
	}
	
	@Test
	public void testSendToMongoDBException() throws Exception {
		try {
			adapter.sendToMongoDB(null);
		} catch (Exception e) {}
	}
}

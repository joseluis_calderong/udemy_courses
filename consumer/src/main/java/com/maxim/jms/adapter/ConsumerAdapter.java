package com.maxim.jms.adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.maxim.exception.MBJSONParserException;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.mongodb.util.JSONParseException;

@Component
public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	
	@SuppressWarnings({ "unchecked", "resource" })
	public void sendToMongoDB(String json) throws MongoException, Exception {
		logger.info("Sending to MongoDB");
		
		// JLC: This is creating a new MongoClient using default values. (localhost:default_port)
		MongoClient dbClient = new MongoClient();
		MongoDatabase db = dbClient.getDatabase("vendor");
		MongoCollection<Document> collection = db.getCollection("contact");
		
		// JLC: Browsing current collection
		logger.info("Using vendor Database");
		logger.info("Current collection has " + collection.count() + " Document Object(s)");
		int i=0;
		for ( Document item : collection.find()) {
			logger.info("Element: " + ++i + item.toJson());
		}
		
		logger.info("Converting JSON to Document Object");
		Document document = null; 
				
		try {
			DBObject dbObject = (DBObject) JSON.parse(json);
			document = new Document(dbObject.toMap());
		} catch (Exception e) {
			if (e instanceof JSONParseException) {
				throw new MBJSONParserException("Exception while parsing to JSON. Offending character: " + e.getMessage()
			+ " Original Exception Type: " + e.getClass(), new Throwable("Invalid JSON message structure."));
			}
		}
		
		collection.insertOne(document);
		
		logger.info("Sent to DB");
		logger.info("Now collection has " + collection.count() + " Document Object(s)");
		dbClient.close();
	}
}

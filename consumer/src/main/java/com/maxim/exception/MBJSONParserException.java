package com.maxim.exception;

public class MBJSONParserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MBJSONParserException() {}
	
	public MBJSONParserException(String message, Throwable cause) {
		super(message, cause);
	}
}
